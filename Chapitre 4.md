---
title: "Ch. 4" 
---

## Chapitre 4:  _Les séries de Fourier_

### 4.1: Motivations, rappels et résultats préliminaires
##### Taylor: 
$$
f\in C^{\infty} \xrightarrow{?}f(x)=\sum_{k=0}^\infty a_{k}\left(x-x_{0}\right)^{k}.
$$
##### Fourier:
$$
\begin{aligned}f, 2\pi-\text{périodique} \xrightarrow{?}f(x)&=\sum_{k=0}^\infty (a_{k}\cos(kx) + b_k\sin(kx))\\&=a_0 + \sum_{k=1}^\infty \left(a_{k}\cos(kx) + b_k\sin(kx)\right)\end{aligned}
$$
##### Définition 4.1: Continuité par morceaux, $C^1$ par morceaux
Soient $a <b, f:[a,b] \to \mathbb R$. $f$ est continue par morceaux et on note $f \in C^0_\text{morc}[a,b]$ si il existe $n \in \mathbb N, a=a_0<a_1<\ldots<a_n=b$ tel que $\forall i = 1, \ldots, n, f\in C^0[a_{i-1}, a_i]$ et $\lim_{x\to a_{i-1}^+}f(x)\stackrel{\text{déf}}{=}f(a_{i-1}^+),\lim_{x\to a_i^-}f(x)\stackrel{\text{déf}}{=}f(a_i^-)$ existent et sont finies.
Si de plus $f\in C^1[a_{i-1}, a_i]$ et $f'(a_i^{\pm})$ existent et sont finies, alors $f$ est $C^1$ par morceaux et on note $f \in C^1_\text{morc}[a,b]$

Par exemple, la fonction de gauche est continue par morceau, mais pas celle de droite puisque toutes les limites à gauche n’existent pas (à cause de l’asymptote verticale) :
![[Pasted image 20231109175644.png]]
##### Proposition 4.2:
Soient $n, m \in \mathbb N_{\geq 1}, T > 0$. Alors 
$$
\frac{2}{T}\int_0^T\cos\left(\frac{2\pi}{T}nx\right)\cos\left(\frac{2\pi}{T}mx\right)dx=\frac{2}{T}\int_0^T\sin\left(\frac{2\pi}{T}nx\right)\sin\left(\frac{2\pi}{T}mx\right)dx = \begin{cases} 1 \text{ si } n = m \\ 0 \text{ sinon}\end{cases}
$$

$$
\int_0^T\sin\left(\frac{2\pi}{T}nx\right)\cos\left(\frac{2\pi}{T}mx\right)dx=0.
$$

##### Remarque 4.3
$V = \{f: \mathbb R \to \mathbb R | f \in C^1_\text{morc}, f\ 2\pi-\text{périodique}\}$ est un espace vectoriel. On le munit du produit scalaire 
$$
\langle f,g\rangle = \frac 1 \pi \int^{2\pi}_0f(x)g(x)dx
$$
Ce que la proposition 4.2 nous dit est que $L = \{ \sin(nx)|n\in\mathbb N_{\geq 1}\} \cup \{ \cos(nx)|n\in\mathbb N_{\geq 1}\}$ est une liste orthonormale
##### Proposition 4.4:
Soit $f: \mathbb R \to \mathbb R, T-\text{périodique}$, continue par morceaux. Alors $\forall a \in \mathbb R$, 
$$
\int^{a + T}_af(x)dx = \int^{T}_0f(x)dx 
$$
### 4.2: Définition et convergence des séries de Fourrier
Soit $f: \mathbb R \to \mathbb R$ T-périodique. Si je peux écrire $f(x) =a_0 + \sum_{k=1}^\infty \left(\alpha_{k}\cos(\frac {2\pi} T kx) + \beta_k\sin(\frac {2\pi} T kx)\right)$
Comment trouver $\alpha_k$ et $\beta_k$ ?
On choisit 
$$
\begin{aligned}\alpha_{k}&=\left\langle f,\cos\left(\frac{2\pi}{T}kx\right) \right\rangle\\&=\frac{2}{T}\int_{0}^{T}f(x)\cos\left(\frac{2\pi}{T}kx\right)dx\end{aligned}
$$
$$
\begin{aligned}\beta_{k}&=\left\langle f,\sin\left(\frac{2\pi}{T}kx\right) \right\rangle\\&=\frac{2}{T}\int_{0}^{T}f(x)\sin\left(\frac{2\pi}{T}kx\right)dx\end{aligned}
$$

##### Définition 4.5: Coefficients de Fourier réels, somme partielle de Fourier et les séries de Fourier
Soit $f: \mathbb R \to \mathbb R$ continue par morceaux, T-périodique, les coefficients de Fourier réels de $f$ sont définis par: $\forall n > 0$ 
$$
a_{n}=\frac{2}{T}\int_{0}^{T}f(x)\cos\left(\frac{2\pi}{T}nx\right)dx
$$
$$
b_{n}=\frac{2}{T}\int_{0}^{T}f(x)\sin\left(\frac{2\pi}{T}nx\right)dx
$$
La somme partielle de Fourier de $f$ et d'ordre $N$ est
$$
F_Nf(x)= \frac{a_{0}}{2}+\sum_{n=1}^{N}\left\{a_{n}\cos\left(\frac{2\pi}{T}nx\right)+b_{n}\sin\left(\frac{2\pi}{T}nx\right)\right\}
$$
La série de Fourier de $f$ 
$$
Ff(x)= \frac{a_{0}}{2}+\sum_{n=1}^{\infty}\left\{a_{n}\cos\left(\frac{2\pi}{T}nx\right)+b_{n}\sin\left(\frac{2\pi}{T}nx\right)\right\}
$$ 
si elle converge
##### Théorème 4.7: Théorème de Dirichlet
Soit $f: \mathbb R \to \mathbb R$ T-périodique $C^1$ par morceaux.
Alors $\forall x \in \mathbb R$
$$
Ff(x)=\frac{f(x-0)+f(x+0)}{2}=\lim_{t\to0}\frac{f(x-t)+f(x+t)}{2}
$$

##### Remarque 4.8:
1) En réalité, l'hypothèse du théorème est plus faible que ça
2) Ca nous donne un outil pour faire deux choses
	1) Approximer une fonction avec des fonctions $C^\infty$
	2) Calculer des séries



##### Définition 4.10: Coefficients de Fourier complexes
Soit $f: \mathbb R \to \mathbb R$ T-périodique $C^0_\text{morc}.$
On définit les coefficients de Fourier complexes par 
$$
c_n=\left\langle f,e^{i\frac{2\pi}{T}nx}\right\rangle=\frac{1}{T}\int_0^Tf(x)e^{-i\frac{2\pi}{T}nx}dx,\quad\text{ pour }n\in\mathbb{Z}
$$
où pour $\varphi: \mathbb R \to \mathbb C$,
$$
\int_{a}^{b}\varphi(x)dx=\int_{a}^{b}\Re(\varphi(x))dx+i\int_{a}^{b}\Im(\varphi(t))dx.
$$

##### Proposition 4.11:
Soit $f: \mathbb R \to \mathbb R$ T-périodique $C^0_\text{morc}.$ Alors,
1) 
$$
\begin{aligned}\forall n \geq 1,\quad c_n &= \frac{a_n-ib_n}{2}\\ \forall n \le - 1,\quad c_n &= \frac{a_{-n}+ib_{-n}}{2}\\ c_0 &= \frac {a_0}2 \end{aligned}
$$
2) 
$$
\begin{aligned}\forall n \geq 1,\quad a_n &= c_n + c_{-n}\\ b_n &= i(c_n-  c_{-n})\\ a_0 &=2{c_0} \end{aligned}
$$
3) 
$$
\begin{gathered}F_{N}f\left(x\right)=\sum_{n=-N}^{N}c_{n}e^{i\frac{2\pi}{T}nx}\\ Ff(x)=\sum_{n=-\infty}^{+\infty}c_{n}e^{i\frac{2\pi}{T}nx}=\lim_{N\to+\infty}\sum_{n=-N}^{N}c_{n}e^{i\frac{2\pi}{T}nx}\end{gathered}
$$
### 4.3: Propriétés des séries de Fourier
##### Proposition 4.12:
Soit $f: \mathbb R \to \mathbb R$ un fonction T-périodique $C^0_\text{morc}.$ Alors,
1) La série de Fourier de $f$, $Ff$ est T-périodique
2) Si $f$ est paire (i.e $f(-x) = f(x)$), $b_n = 0\quad \forall n \geq 1$ et 
$$
Ff(x) = \frac {a_0} 2 + \sum_{n=1}^{\infty}a_{n}\cos\left(\frac{2\pi}{T}nx\right)
$$
3) Si $f$ est impaire (i.e $f(-x) = -f(x)$), $a_n = 0\quad \forall n \geq 0$ et 
$$
Ff(x) = \sum_{n=1}^{\infty}b_{n}\sin\left(\frac{2\pi}{T}nx\right)
$$
##### Proposition 4.13: Série de Fourier en cosinus
Soit $L > 0,\ f:[0,L] \to \mathbb R$ une fonction $C^1_\text{morc}$ et la série
$$
F_Cf(x) = \frac{\tilde{a_o}}2 + \sum_{n=1}^{\infty}\tilde{a_{n}}\cos\left(\frac{\pi}{L}nx\right)
$$
où 
$$
\tilde{a_n} = \frac 2 L \int^L_0f(x)cos\left(\frac \pi L nx\right)dx
$$
Alors $F_Cf$ converge vers
$$
F_{C}f(x)=\begin{cases}\frac{f(x-0)+f(x+0)}{^{2}}&\text{si }x \in\mathrm{]0,L[}\\f(0+0)&\text{si } x = 0\\f(L-0)&\text{si } x=L.\end{cases}
$$
##### Proposition 4.14: Série de Fourier en sinus
Soit $L > 0,\ f:[0,L] \to \mathbb R$ une fonction $C^1_\text{morc}$ et la série
$$
F_Sf(x)=\sum_{n=1}^\infty\widetilde{b}_n\sin\Bigl(\frac{\pi}{L}nx\Bigr)
$$
où
$$
\begin{aligned}\widetilde{b}_n=\frac{2}{L}\int_0^Lf(x)\sin\!\left(\frac{\pi}{L}nx\right)dx\end{aligned}
$$
Alors $F_Sf$ converge vers
$$
F_{S}f(x)=\begin{cases}\frac{f(x-0)+f(x+0)}{^{2}}&\text{si }x \in\mathrm{]0,L[}\\0&\text{si } x = 0 \text{ ou } x=L\end{cases}
$$


##### Théorème 4.16: L'identité de Parseval 
Soit $f: \mathbb R \to \mathbb R$ une fonction $C^1_\text{morc}$. Alors, 
$$
\frac{2}{T}\int_{0}^{T}f^{2}(x)dx=\frac{a_{0}^{2}}{2}+\sum_{n=1}^{+\infty}(a_n^{2}+b_n^{2}) = 2\sum_{n\in \mathbb Z}|c_n|^2
$$
où $\{a_n\}, \{b_n\}$ sont les coefficients de Fourier réels de F et $\{c_n\}$ sont les coefficients complexes
##### Remarque 4.17:
1. Le théorème reste vrai si on affaiblit l'hypothèse, $|f|, f^2$ sont intégrables sur $[0,T]$, le résultat reste vrai
2. Une deuxième méthode pour calculer des séries
##### Remarque 4.19:
Un exercice typique sur les séries de Fourier est la donnée d'une fonction $f: \mathbb R \to \mathbb R$ T-périodique et des questions:
- Calculer la série de Fourier de f
- En déduire la valeur d'une série $\sum_{n=1}^\infty\varphi_n$

Pour déduire la valeur de la **série** $\sum_{n=1}^\infty\varphi_n$ on a deux **possibilités**:
- Théorème de Dirichlet
- Identité de Parseval
Indicateur pour savoir quelle méthode choisir:
Comparer l'ordre de $n$ dans $a_n$ et $b_n$ et l'ordre de $n$ dans $\varphi_n$
- Si ordre $a_n$ et $b_n$ = ordre $\varphi_n$ => Théorème de Dirichlet
- Si 2 $\times$ ordre $a_n$ et $b_n$ = ordre $\varphi_n$ => Identité de Parseval
##### Proposition 4.20: Dérivation et intégration terme à terme des séries de Fourier
Soit $f: \mathbb R \to \mathbb R$, T-périodique, continue, $C^1_\text{morc}$ telle que $f'\in C^1_\text{morc}$. Alors
$$
Ff^{\prime}=\sum_{n=1}^{\infty}\frac{2\pi}{T}n\left\{-a_n\sin\left(\frac{2\pi}{T}nx\right)+b_n\cos\left(\frac{2\pi}{T}nx\right)\right\} = \frac{f^{\prime}(x-0)+f^{\prime}(x+0)}2
$$
où $\{a_n\}$ et $\{b_n\}$ sont les coefficients de Fourier de $f$
##### Remarque 4.21
$f'$ n'est en fait pas définie partout. Il y a des points isolés où $f$ n'est pas définie (là ou $f$ n'est pas dérivable). Ca n'est plus un problème car:
1. Si je calcule les coefficients de Fourier de $f'$, je calcule des intégrales. Et ce qui se passe sur un point isolé est négligeable
2. $\frac{f'(x-0) + f'(x+0)} 2$ est défini partout!
##### Remarque 4.21.2
$f$ est $T$-périodique $\implies$ $f'$ est $T$-périodique

$f$ est $T$-périodique et $F' = f$ $\;\not\!\!\!\!\implies$ $F$ est $T$-périodique

$f$ est $T$-périodique et $\int_0^Tf(x)dx= 0$ et $F' = f$ $\implies$ $F$ est $T$-périodique

