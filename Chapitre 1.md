---
title: "Ch. 1" 
---
## Chapitre 1: _Les operateurs différentiels_
##### Définition 1.1: Le gradient
Soit $\Omega \subseteq \mathbb R^n$ un ouvert et $f \in C^1(\Omega)$.
Le **gradient** de $f$ noté $\operatorname{grad}f$, $\nabla f$ ou $\operatorname{D}f$ est le champs vectoriel défini par $\nabla f: \Omega \to \mathbb R^n$

$$
x \to \nabla f(x) := \left(\frac {\partial f}{\partial x_1}(x), \frac {\partial f}{\partial x_2}(x), ..., \frac {\partial f}{\partial x_n}(x)   \right)
$$
##### Remarque 1.2:
On écrit $\nabla = \left(\frac {\partial}{\partial x_1}(x), \frac {\partial}{\partial x_2}(x), ..., \frac {\partial}{\partial x_n}(x)   \right)$ pour que $\nabla f$ se comporte comme une " multiplication par scalaire" du "vecteur" $\nabla$ avec le scalaire $f$. On interprète 
$$
\frac \partial{\partial x_i} \cdot f = \frac {\partial f}{\partial x_i}
$$
##### Définition 1.4: La divergence
Soit $\Omega \subseteq \mathbb R^n$ un ouvert et $F \in C^1(\Omega; \mathbb R^n)$,
La divergence de F notée div$F, \nabla \cdot F, <\nabla, F>$ est le champs scalaire défini par $\operatorname{div} F: \Omega \to \mathbb R$
$$
x \to \operatorname{div} F(x) = \sum_{i = 1}^n\frac {\partial F_{i}}{\partial x_i}(x) = \frac {\partial F_{1}}{\partial x_1}(x) + ... + \frac {\partial F_{n}}{\partial x_n}(x)
$$
##### Définition 1.6: Le rotationnel
Soit $\Omega \subseteq \mathbb R^n$ un ouvert et $F \in C^1(\Omega; \mathbb R^n)$. Alors le rotationnel de $F$ noté $\operatorname{rot}F$ est défini par:
Si $n=2$ et $F(x) = (F_{1}(x), F_{2}(x)), F \in C^1(\Omega, \mathbb R^2)$, alors $\operatorname{rot}F$ est défini par
$$
\operatorname{rot}F =  \frac{\partial F_{2}}{\partial x_{1}} - \frac{\partial F_{1}}{\partial x_{2}} \in \mathbb R
$$
Si $n=3$ et $F(x) = (F_{1}(x), F_{2}(x), F_{3}(x)), F \in C^1(\Omega, \mathbb R^3)$, alors $\operatorname{rot}F$ est défini par
$$
\operatorname{rot}F=\left(\frac{\partial F_3}{\partial x_2}-\frac{\partial F_2}{\partial x_3},\frac{\partial F_1}{\partial x_3}-\frac{\partial F_3}{\partial x_1},\frac{\partial F_2}{\partial x_1}-\frac{\partial F_1}{\partial x_2}\right)\in\mathbb{R}^3
$$
Technique pour le calcul de $\operatorname{rot}F$ dans le cas $n=3$
$$
\operatorname{rot}F=\left|\begin{matrix}{}e_1&e_2&e_3\\\frac{\partial}{\partial x_1}&\frac{\partial}{\partial x_2}&\frac{\partial}{\partial x_3}\\F_1&F_2&F_3\end{matrix}\right|
$$
##### Définition 1.8: Le Laplacien
Soit $\Omega \subseteq \mathbb R^n$ un ouvert et $f \in C^2(\Omega)$,
Le Laplacien de f noté $\Delta f$ est défini par $\Delta f: \Omega \to \mathbb R$
$$
x \to \Delta F(x) = \sum_{i = 1}^n\frac {\partial^2 f}{\partial x_i^2}(x) = \frac {\partial^2 f}{\partial x^2_1}(x) + ... + \frac {\partial^2 f}{\partial x_n^2}(x)
$$
##### Théorème 1.10:
Soit $\Omega \subseteq \mathbb R^n$ un ouvert et $f \in C^2(\Omega), F \in C^2(\Omega; \mathbb R^n$). Alors,
1. $\operatorname{div}(\nabla f) = \Delta f\quad\forall n \geq 2$
2. $\operatorname{rot}(\nabla f) = 0\quad n = 2,3$
3. $\operatorname{div}(\operatorname{rot }F) = 0$ 