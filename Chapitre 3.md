---
title: "Ch. 3"
---
## Chapitre 3: _Intégrales de surface_

##### Notation 3.1:
A partir de maintenant on se permet d'écrire les composantes de nos applications avec les indices en haut:
$$
G(\bar x) = (G^1(\bar x), G^2(\bar x), \ldots, G^n(\bar x))
$$
On écrira aussi pour $f: \mathbb R^3\to \mathbb R$ 
$$
f_x(x,y,z) = \frac{\partial f}{\partial x}(x,y,z),\ \ f_y(x,y,z) = \frac{\partial f}{\partial y}(x,y,z),\ \ f_z(x,y,z)= \frac{\partial f}{\partial z}(x,y,z)
$$
##### Définition 3.2: Surface régulière
$\Sigma \subseteq \mathbb R^3$ est une surface régulière si:
1. $\exists A \subseteq \mathbb R^2$ un ouvert borné tel que $\partial A$ est une courbe simple fermée régulière (par morceaux) et $\sigma: \bar A \to \mathbb R^3: (u,v) \to \sigma(u,v) = (\sigma^1(u,v), \sigma^2(u,v), \sigma^3(u,v))$ telle que
	- $\sigma \in C^1(\bar A, \mathbb R^3)$
	- $\sigma(\bar A) = \Sigma$
	- $\sigma$ est injective sur $A$
2.  <br>

	$$
	\begin{aligned}
	\vec{\sigma_u} \wedge \vec{\sigma_v} =\begin{vmatrix}
	\bar e_1 & \bar e_2 & \bar e_3\\
	\sigma_u^1 & \sigma_u^2 & \sigma_u^3\\
	\sigma_v^1 & \sigma_v^2 & \sigma_v^3
	\end{vmatrix} = \begin{bmatrix}
	\sigma_u^2 \sigma_v^3 - \sigma_v^2 \sigma_u^3 \\
	\sigma_v^1 \sigma_u^3 - \sigma_u^1 \sigma_v^3 \\
	\sigma_v^2 \sigma_u^1- \sigma_u^2 \sigma_v^1
	\end{bmatrix}
	\end{aligned}
	$$

	est tel que $|\sigma_u \wedge \sigma_v| \neq 0\ \ \forall(u,v) \in A$

$\sigma$ est une paramétrisation régulière de $\Sigma$ et $\frac {\sigma_v \wedge \sigma_u}{|\sigma_v \wedge \sigma_u|} = \nu_{(u,v)}$ est la normale unité au point $\sigma_{(u,v)}$

##### Définition 3.4: Surface orientable
Une surface régulière $\Sigma \subseteq \mathbb R^3$ est orientable si il existe un champ de vecteurs normaux unitaires continus
$$
\nu : \Sigma \to \mathbb R^3
$$
La donnée d'un tel champ est appelée une orientation de $\Sigma$
<div style="text-align: center">
![[Pasted image 20231029160942.png|300]]
<br>
Figure ci dessus: Surfaces non-orientables
</div>

##### Remarque 3.5:
Les problèmes avec la continuité de $\nu$ peut survenir au "recollement". C'est à dire sur les parties de $\partial \mathcal A$ où $\sigma$ n'est pas injective.
Les surfaces non orientables sont très rares.

##### Définition 3.6: Intégrale de surface
Soit $\Omega \subseteq \mathbb R^3$ un ouvert, $f\in C^0(\Omega), F\in C^0(\Omega;\mathbb R^3), \Sigma \subseteq \Omega$ une surface orientable paramétrée par $\sigma: \bar{\mathcal A} \to \Sigma: (u,v) \mapsto \sigma(u,v)$ 
L'intégrale de $f$ sur $\Sigma$ est définie par
$$
\iint_\Sigma fdS=\iint_Af(\sigma(u,v))\|\sigma_u\wedge\sigma_v\|dudv
$$
L'intégrale de $F$ sur $\Sigma$ est définie par 
$$
\iint_\Sigma F\bullet dS=\iint_AF(\sigma(u,v))\bullet(\sigma_u\wedge\sigma_v)dudv
$$

Si $\Sigma = \bigcup_{i = 1}^k \Sigma_i$ est une surface régulière par morceaux avec $\Sigma_i$ régulière orientable,
$$
\iint_{\Sigma}f dS = \sum_{i = 1}^k\iint_{\Sigma_i}f dS,\quad \quad \iint_{\Sigma}F \bullet dS = \sum_{i = 1}^k\iint_{\Sigma_i}F \bullet dS
$$
##### Remarque 3.7:
En comparant avec l'intégrale curviligne,
$$
\gamma \longleftrightarrow \sigma, \quad \gamma'(t) \longleftrightarrow \sigma_u \wedge \sigma_v
$$
L'intégrale de surface d'un champ scalaire ne dépend pas du choix de la paramétrisation.
Le signe de l'intégrale de surface d'un chams vectoriel dépend de l'orientation de $\sigma_u \wedge \sigma_v$ i.e le choix d'orientation de $\Sigma$, plus précisément ud choix entre $\sigma_u \wedge \sigma_v$ et $\sigma_v \wedge \sigma_u$

##### Définition 3.9: Domaine régulier de $\mathbb R ^3$
Soit $\Omega\in\mathbb{R}^3$ un ouvert borné. $\Omega$ est un domaine régulier s'il existe des ouverts bornés $\Omega_0,\ldots,\Omega_m$ tels que :
1. $\overline {\Omega_i}\subseteq \Omega_0$ pour $1\leq i\leq m$.
2. $\overline {\Omega_{i}}\cap \overline {\Omega_{j}}= \phi$ pour $1\leq i< j\leq m$
3. $\Omega=\Omega_0\setminus\bigcup_{i=1}^k\overline{\Omega_i}$
4. $\partial\Omega_i= \Sigma_i$ est une surface régulière par morceaux orientable


##### Théorème 3.10: Théorème de la divergence
Soient $\Omega\subset\mathbb{R}^3$ régulier, $F\in C^1(\overline{\Omega},\mathbb{R}^3)$  $\nu$ un champ de normales extérieures à $\Omega$ continu. Alors:
$$
\iiint_{\Omega}\operatorname{div}(F)dxdydz=\iint_{\partial\Omega}(F\bullet\nu)dS
$$
##### Remarque 3.12
Si $\Sigma$ est une partie du bord de $\Omega$ paramétrée par $\sigma$, alors
$$
\begin{aligned}
\iint_{\Sigma}(F\bullet\nu)dS& =\iint_A(F\bullet\nu)\|\sigma_u\wedge\sigma_v\|dudv  \\
&=\pm\iint_AF\bullet\frac{(\sigma_u\wedge\sigma_v)}{\|\sigma_u\wedge\sigma_v\|}\|\sigma_u\wedge\sigma_v\|dudv \\
&=\pm\iint_AF\bullet(\sigma_u\wedge\sigma_v)dudv \\
&=\pm\iint_{\Sigma}F\bullet dS \\
&\iint_{\Sigma}(F\bullet\nu)dS=\pm\iint_{\Sigma}F\bullet dS
\end{aligned}
$$
où le signe $\pm$ est choisi de telle sorte que $\pm \sigma_u\wedge\sigma_v$ est un vecteur normal extérieur
##### Corollaire 3.13: Formules d'aire
Soit $\Omega \subseteq \mathbb R^3$ un domaine régulier et $\nu: \partial \Omega \to \mathbb R^3$ un champ de normales extérieures unités continu. Soient encore les champs vectoriels
$F(x,y,z) = (x,y, z);\quad G_1(x,y,z) = (x,0,0);$
$G_2(x,y,z) = (0,y,0);\quad G_3(x,y,z) = (0,0,z)$
Alors:
$$
\text{Volume}(\Omega)  = \frac 1 3 \iint _{\partial \Omega} (F\bullet\nu)dS =  \iint _{\partial \Omega} (G_i\bullet\nu)dS
$$ 
##### Corollaire 3.14: Identités de Green
Soit $\Omega \subseteq \mathbb R^3$ un domaine régulier , $f, g \in C^2(\bar\Omega)$ $\nu: \partial \Omega \to \mathbb R^3$ un champ de normales extérieures unités continu. Alors:
$$
\iiint_\Omega(\Delta f) dxdydz=\iint_{\partial\Omega}\langle \nabla f;\nu \rangle ds
$$
$$
\iiint_\Omega(f\Delta g+\langle\nabla f;\nabla g\rangle)dxdydz=\iint_{\partial\Omega}\langle f\nabla g;\nu \rangle ds
$$
$$
\iiint_\Omega(f\Delta g-g\Delta f)dxdydz=\iint_{\partial\Omega}\langle f\nabla g-g\nabla f;\nu \rangle ds
$$

##### Définition 3.15: Bord d'une surface, sens de parcours induit par $\sigma$
Soit $\Sigma\subseteq \mathbb R^3$ une surface régulière orientable, $\sigma: \bar{\mathcal A} \to \Sigma$ une paramétrisation de cette surface
1. Si $\partial \mathcal A$ est une courbe simple, régulière, fermée alors:
$$
\partial \Sigma = \sigma(\partial\mathcal A)
$$ 
et le choix d'un sens de parcours sur $\partial\mathcal A$ induit un sens de parcours sur $\partial \Sigma$ par composition avec $\sigma$: si $\gamma: [a,b] \to \mathbb R^2$ est une paramétrisation de $\partial \mathcal A$ (et donc, un choix de sens de parcours), $\sigma \circ \gamma: [a,b] \to \mathbb R^3$ est une paramétrisation de $\partial \Sigma$ et donc un choix de sens de parcours. Le sens de parcours de $\sigma \circ \gamma$ est le sens de parcours induit par $\sigma$
2. Si  $\partial \mathcal A$ contient une  courbe simple fermée régulière par morceaux, le bord de $\Sigma$ est donné par $\sigma \partial \mathcal A$ dont on **enlève**:
	1. Les bouts de courbes qui sont parcourues deux fois dans des directions opposés
	2. Les bouts de courbes qui sont réduits à un point
	**L'orientation induite par $\sigma$** est définie de manière analogue à ci-dessus

##### Remarques 3.17:
1. Il n'existe pas de bonne notion de bord orienté positivement pour une surface $\Sigma \subseteq \mathbb R^3$
2. Fun fact: $\partial(\partial \Omega) = \emptyset$

##### Théorème 3.18: Théorème de Stokes
Soit $\Omega \subseteq \mathbb R^3$ un ouvert, $\Sigma\subseteq \Omega$ une surface régulière orientable par morceaux,  $F\in C^1(\overline{\Omega},\mathbb{R}^3)$. Alors
$$
\iint_\Sigma(\operatorname{rot}F)\bullet dS=\int_{\partial\Sigma}F\bullet d\ell
$$
##### Remarques 3.19:
1. Le signe de $\iint_\Sigma(\operatorname{rot}F)\bullet dS$ dépend de l'orientation de $\Sigma$ (intégrale de surface d'un champ vectoriel). Le signe de $\int_{\partial\Sigma}F\bullet d\ell$ dépend du sens de parcours (intégrale curviligne d'un champ vectoriel). Astuce pour s'assurer que les choix qu'on fait sont compatibles: 
	![[Pasted image 20231108130304.png|500]]
2. $\iint_{\partial\Omega}(\operatorname{rot}F)\bullet dS = 0 \quad \forall \Omega \subseteq \mathbb R ^3 \text{ domaine régulier}$