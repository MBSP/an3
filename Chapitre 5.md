---
title: "Ch. 5"
---
## Chapitre 5: _Tranformée de Fourier_
### § 5.1: Définition et inversion
##### Définition 5.1: Transformée de Fourier, transformée de Fourier inverse
- 
	Soit $f: \mathbb R \to \mathbb R$ une fonction continue par morceaux telle que 
	
	<br>

	$$
	\int_{-\infty}^{\infty}|f(x)|dx < \infty
	$$
	<br>

	La transformé de Fourier de $f$ noté $\mathcal F(f)$ ou $\hat f: \mathbb R \to \mathbb C$ est  définie par 
	
	<br>

	$$
	\mathcal F(f)(\alpha) = \hat f(\alpha) = \frac 1 {\sqrt{2\pi}} \int_{-\infty}^{\infty}f(x)e^{-i\alpha x}dx
	$$
	<br>

- 
	Soit $\varphi: \mathbb R \to \mathbb C$ une fonction continue par morceaux telle que 

	<br>

	$$
	\int_{-\infty}^{\infty}|\varphi(\alpha)|d\alpha < \infty
	$$

	<br>

	La transformée de Fourier inverse de $\varphi$ notée $\mathcal F^{-1}(\varphi) : \mathbb R \to \mathbb C$ est définie par 
	
	<br>

	$$
	\mathcal F^{-1}(\varphi)(x) = \frac 1 {\sqrt{2\pi}} \int_{-\infty}^{\infty}\varphi(\alpha)e^{i\alpha x}d\alpha
	$$

	<br>


##### Théorème 5.2: Formule d'inversion
Soit $f: \mathbb R \to \mathbb R$ une fonction continue par morceaux telle que $\int_{-\infty}^{\infty}|f(x)|dx < \infty$ et $\int_{-\infty}^{\infty}|\hat f(\alpha)|d\alpha < \infty$. Alors
$$
f(x) = \mathcal F^{-1}(\mathcal F(f))(x) = \frac 1{\sqrt{2\pi}}\int_{-\infty}^{\infty}\hat f(\alpha)e^{i\alpha x}d\alpha
$$
##### Remarque 5.4:
En réalité calculer une transformée de Fourier, c'est dur. Il nous faut des outils de l'analyse complexe pour calculer la plupart des transformées de Fourier
### § 5.2 Propriétés des transformées de Fourier
##### Proposition 5.5: Continuité, linéarité, composition avec fonctions affines et décalage
Soient $f,g: \mathbb R \to \mathbb R$ continues par morceaux telles que $\int_{-\infty}^{\infty}|f(x)|dx < \infty$ et $\int_{-\infty}^{\infty}|g(x)|dx < \infty$. Alors
1. **Continuité**: $\hat f$ est continue
2. **Linéarité**: $\mathcal F (af + bg) = a \mathcal F (f) + b \mathcal F (g) \quad \forall a,b \in \mathbb R$
3. **Composition avec fonction affine**: Si $a \neq 0, b \in \mathbb R$  et $g(x) = f(ax + b)$ alors $\hat g(\alpha) = \frac{e^{i\frac b a\alpha}}{|a|}\hat f(\frac \alpha a)$
4. **Décalage**: Si $g(x) = e^{-ibx}f(x)$ alors $\mathcal F(g)(\alpha) = \mathcal F (f)(\alpha + b)$

##### Théorème 5.6: Identité de Plancherel
Soit $f$ continue par morceaux telle que $\int_{-\infty}^{\infty}|f(x)|dx < \infty$ et $\int_{-\infty}^{\infty}\hat{f}(\alpha)^2d\alpha < \infty$. Alors
$$
\int_{-\infty}^{\infty}f(x)^2dx = \int_{-\infty}^{\infty}\hat f(\alpha)^2d\alpha
$$
##### Proposition 5.7: Transformée des dérivées et dérivées des transformées
1.
	Soit $f \in C^1(\mathbb R)$ telle que $\int_{-\infty}^{\infty}|f'(x)|dx < \infty$ $\int_{-\infty}^{\infty}|f(x)|dx < \infty$. Alors, 
		
	<br>
	
	$$
	\mathcal F(f')(\alpha) = i\alpha \mathcal F(f)(\alpha)
	$$
		
	<br>
	
	Plus généralement, si $\int_{-\infty}^{\infty}|f(x)^{(k)}|dx < \infty\quad \forall1\leq k \leq n$. Alors
		
	<br>
	

	$$
	\mathcal F(f^{n})(\alpha) = (i\alpha)^n \mathcal F(f)(\alpha)
	$$
		
	<br>
	
2. 
	Soit $f$ continue par morceaux et $h: \mathbb R \to \mathbb R$ définie par $h(x) = xf(x)$ telles que $\int_{-\infty}^{\infty}|f(x)|dx < \infty$ et $\int_{-\infty}^{\infty}|h(x)|dx < \infty$. Alors, 
		
	<br>
	

	$$
	\hat f'(\alpha)= -i\mathcal F(h)(\alpha)
	$$ 
		
	<br>
	
	Plus généralement, si $h_k = x^kf(x) \text { et que}  \int_{-\infty}^{\infty}|h(x)|dx < \infty\quad \forall1\leq k \leq n$. Alors
		
	<br>
	
	$$
	\begin{aligned}\frac{d^n\hat f}{d\alpha^n}(\alpha) &= (-i)^n\mathcal F(h_n)(\alpha)\\ &= (-i)^n \frac 1 {\sqrt{2\pi}} \int_{-\infty}^{\infty}x^nf(x)e^{-i\alpha x}dx\end{aligned}
	$$
		
	<br>
	
##### Proposition 5.9: Transformée en sinus ou cosinus
Soit $f:\mathbb R \to \mathbb R$ telle que $\int_{-\infty}^{\infty}|f(x)|dx < \infty$
1. Si $f$ est paire, alors
$$
\mathcal F(f)(\alpha) = \sqrt{\frac{2}{\pi}}\int_{0}^{+\infty}f(x)\cos(\alpha x)dx
$$
2. Si $f$ est impaire, alors
$$
\mathcal F(f)(\alpha)=-i\sqrt{\frac{2}{\pi}}\int_{0}^{+\infty}f(x)\sin(\alpha x)dx
$$
##### Remarque 5.10:
1. Au vu des formules on a que 
	- si $f$ est pair, $\hat f$ l'est aussi
	- si $f$ est impair, $\hat f$ l'est aussi
	On a donc, si on garde l'hypothèse que $f$ est continue et $\int_{-\infty}^{\infty}|f(x)|dx < \infty$ que
	 $$
	\begin{cases}f(x) &=\sqrt{\frac{2}{\pi}}\int_{0}^{+\infty}\hat f(x)\cos(\alpha x)dx \quad \text { si f est paire}\\ f(x) &=i\sqrt{\frac{2}{\pi}}\int_{0}^{+\infty}\hat f(x)\sin(\alpha x)dx \quad \text { si f est impaire}\end{cases}
	$$
2. Ceci nous donne aussi des outils de transformée de Fourier pour pour des fonctions $f:[0, +\infty[\ \to \mathbb R$ en les étendant à $\mathbb R$ de façon paire ou impaire
##### Définition 5.11: Produit de convolution
Soient $f,g: \mathbb R \to \mathbb R$ telles que $\int_{-\infty}^{\infty}|f(x)|dx < \infty$ $\int_{-\infty}^{\infty}|g(x)|dx < \infty$. Le produit de convolution de $f$ et $g$ est défini par
$$
(f * g)(x) = \int_{-\infty}^{+\infty}f(x-t)g(t)dt
$$
##### Proposition 5.12: transformée de produit de convolution
Soient $f,g: \mathbb R \to \mathbb R$ telles que $\int_{-\infty}^{\infty}|f(x)|dx < \infty$ et $\int_{-\infty}^{\infty}|g(x)|dx < \infty$. Alors $\int_{-\infty}^{+\infty}|f * g(x)|dx < \infty$ et 
$$
\mathcal F(f * g)(\alpha) = \sqrt{2\pi}\mathcal F(f)(\alpha) \mathcal F(g)(\alpha)
$$
