---
title: "Ch. 2"  
---
## Chapitre 2: _Les intégrales curvilinges_


##### Définition 2.1: Courbes régulières, courbes simples régulières, paramétrisation
$\Gamma \subseteq \mathbb R^n$ est une courbe **régulière** si il existe $[a,b] \subseteq \mathbb R$ et une fonction $\gamma[a,b] \to \mathbb R^n : t \to \gamma(t) = \gamma_1(t), ... \gamma_n(t)$ telle que
1. $\gamma([a,b])=\{x\in\mathbb{R}^n:\exists t\in[a,b]\mathrm{~avec~}\gamma(t)=x\} = \Gamma$
2. $\gamma \in C^{1}([a,b ];\mathbb R^{n})$
3. $\left\|\gamma^{\prime}\left(t\right)\right\|=\sqrt{\sum_{j=1}^{n}\left(\gamma_{j}^{\prime}\left(t\right)\right)^{2}} \neq 0$
$\gamma$ est alors une **paramétrisation** de $\Gamma$
De plus $\Gamma$ est une courbe **simple** s'il existe une paramétrisation de $\Gamma$ telle que $\gamma$ est injective sur $[a,b[$ 
De plus $\Gamma$ est une courbe **fermée** si toutes les paramétrisations régulières de $\Gamma$ vérifient  que $\gamma(a) = \gamma(b)$
On dit que $\Gamma$ est une courbe régulière par morceaux si $\Gamma=\Gamma_1\cup\ldots\cup\Gamma_k$ où les $\Gamma_i$ sont des courbes régulières
##### Définition 2.4 : Intégrale curviligne
Soient $\Omega\subset\mathbb{R}^n$ un ensemble ouvert $\Gamma\subset\Omega$ une courbe régulière, et $\gamma:[a,b]\to\Gamma$ une paramétrisation de cette courbe.
Pour $f\in C^0( \Omega)$ , nous définissons l'intégrale curviligne de $f$ le long de $\Gamma$: 
$$
\int_\Gamma fd\ell=\int_a^bf(\gamma(t))\|\gamma^{\prime}(t)\|dt
$$
Pour un champ vectoriel $F\in C^0(\Omega,\mathbb{R}^n)$ nous définissons :
$$
\int_\Gamma Fd\ell=\int_\Gamma F\bullet d\ell=\int_a^bF(\gamma(t))\bullet\gamma^{\prime}(t)dt
$$
Si $\Gamma = \bigcup _{i = 1}^k \Gamma _k$ 
$$
\int_\Gamma fd\ell=\sum_{i=1}^k\int_{\Gamma_i}fd\ell \quad \text{et}\quad \int_\Gamma F\bullet d\ell=\sum_{i=1}^k\int_{\Gamma_i}F \bullet d\ell
$$
##### Remarque 2.5: Sens et longueur
1. Pour une courbe régulière $\Gamma$, on définit sa longueur par 
$$
\text{long }\Gamma =  \int_\Gamma 1d\ell
$$
2. La notion d'intégrale curviligne d'un champs scalaire est indépendante du choix de paramétrisation. 
	Par contre la notion d'intégrale curviligne d'un champs non scalaire est indépendante du choix de paramétrisation à un signe près
##### Définition 2.7: Champs qui dérivent d'un potentiel
Soient $\Omega\subset\mathbb{R}^n$ un ensemble ouvert, et $F\in C^0(\Omega;\mathbb{R}^n)$.
Alors, le champ vectoriel $F$ dérive du potentiel $f\in C^1(\Omega)$ si $F=\nabla f$.
##### Proposition 2.8
Si $F\in C^1(\Omega;\mathbb{R}^n)$ dérive du potentiel $f\in C^1(\Omega)$ et $\Gamma$ est une courbe régulière de paramétrisation $\gamma: [ a, b] \mapsto \Omega$, alors: 
$$
\int_\Gamma F\bullet d\ell=f(\gamma(b))-f(\gamma(a))
$$
##### Théorème 2.10
Soient $\Omega\subset\mathbb{R}^n$ un ensemble ouvert, et $F\in C^1(\Omega;\mathbb{R}^n)$.
Condition nécessaire: si $F$ dérive d'un potentiel sur $\Omega$ alors
$$
\begin{aligned}\frac{\partial F_i}{\partial x_j}\left(x\right)-\frac{\partial F_j}{\partial x_i}\left(x\right)&=0,\quad\forall i,j=1,\cdots,n\text{ et }\forall x\in\Omega.\end{aligned}
$$
Condition suffisante: si la condition précédente est vérifiée et que $\Omega$ est simplement connexe, alors $F$ dérive d'un potentiel

##### Théorème 2.12:
Soient $\Omega\subset\mathbb{R}^n$ un ensemble ouvert, et $F\in C^0(\Omega;\mathbb{R}^n)$ un champ vectoriel. Alors les affirmations suivantes sont équivalentes:
1. $F$ dérive d'un potentiel sur $\Omega$
2. $\forall A,B \in \Omega$ et $\Gamma_1, \Gamma_2 \subseteq \Omega$ des courbes régulières joignant A à B on a 
$$
\int_{\Gamma_1}F\cdot d\ell=\int_{\Gamma_2}F\cdot d\ell.
$$
3. $\int_\Gamma F\cdot d\ell=0$ pour toute courbe $\Gamma$ fermée simple régulières

##### Remarque 2.13: Marche à suivre pour répondre à la question "est-ce que $f$ dérive d'un potentiel?"
![[Pasted image 20231019193137.png]]
**REPONSES JAMAIS ACCEPTEES:**
- $F$ ne dérive pas d'un potentiel car je ne trouve pas de $f$ tel que $\nabla f  = F$
- $F$ dérive d'un potentiel car je ne trouve pas de $\Gamma \subseteq \Omega$ régulière fermée telle que $\int_\Gamma F \bullet d\ell \neq 0$
##### Définition 2.15: Bord, adhérence, bord orienté positivement/négativement, domaine régulier
1. Soit $\Omega \subseteq \mathbb R^n$ un ouvert borné. Le **bord** de $\Omega$ noté $\partial\Omega$ est
$$
\partial\Omega=\left\{x\in\mathbb{R}^n|\forall r>0,B_r(x)\cap\Omega\neq\phi\text{ et }B_r(x)\cap\Omega^C\neq\phi\right\}
$$
où $B_r(x)$ est la boule ouverte de rayon $r$ centrée en $x$.
On note $\bar \Omega = \Omega \cup \partial \Omega$ l'**adhérence** de $\Omega$

2. Soit $\Omega \subseteq \mathbb R^2$ un ouvert borné tel que $\partial \Omega$ est une courbe simple fermée régulière.
Le bord $\partial \Omega$ est orienté positivement si les paramétrisations choisies laissent le
domaine $\Omega$ à gauche.


3. $\Omega \subseteq \mathbb R^2$ un ouvert borné est un domaine régulier si il existe des ouverts bornés $\Omega_0, \Omega_1, \ldots, \Omega_n$ tq
	- $\bar \Omega_i\subseteq \Omega_0$ pour tout $1\leq i\leq k$.
	- $\bar\Omega_i \cap \bar\Omega_j= \phi$ pour $1\leq i< j\leq k.$
	-  $\partial\Omega_i$ est une courbe régulière simple fermée pour tout $0\leq i\leq k.$
	- $\Omega=\Omega_0\setminus\bigcup^k_{i = 1}\Omega_i$ 


##### Théorème 2.16: Théorème de Green
Soit $\Omega\subseteq\mathbb{R}^2$ un domaine régulier avec le bord $\partial\Omega$ orienté **positivement**. Soit aussi $F\in C^1(\Omega;\mathbb{R}^2).$ Alors :
$$
\iint_{\Omega}\operatorname{rot}F(x,y)dxdy=\int_{\partial\Omega}F\bullet d\ell
$$
##### Remarques 2.17:
1. On retrouve bien la structure 
$$
\int_{\text{intérieur}} \text{dérivées} = \int_{\text{bord}} \text{fonction}
$$ 
ce qui est similaire au théorème fondamental du calcul intégral.
$$
\int_a^b f'(x) = f(b) - f(a)
$$
2. Si $F$ dérive d'un potentiel, le théorème se lit $0 = 0$


##### Définition 2.19: Normale extérieure
Soit $\Omega\subseteq\mathbb{R}^2$ un domaine régulier, $x_0 \in \partial \Omega$. $\nu_{x_0} \in \mathbb R^2$ est la normale extérieure unité si
1. $|{\nu_{x_{0}}}|=1$
2. Si $\gamma: [ a, b] \mapsto \partial\Omega$ est une paramétrisation telle que $\gamma(t_0)$ existe (où $t_0$ est tel que $\gamma( t_0) = x_0)$, alors $\langle\gamma^\prime( t_0), \nu_{x_0}\rangle\ = 0$.
3. Pour tout $\varepsilon> 0$ suffisamment petit, alors $x_0 +\varepsilon\nu_{x_0}\notin\Omega$

Si $\Omega$ est un domaine régulier de bord régulier orienté positivement, et $\gamma:[a,b]\mapsto\partial\Omega$ est une paramétrisation régulière, alors pour tout $t\in[a,b],$ nous avons :
$$
\nu_{\gamma(t)}=\frac{(\gamma_2^{\prime}(t),-\gamma_1^{\prime}(t))}{\|\gamma^{\prime}(t)\|}
$$

##### Corollaire 2.20: Le théorème de la divergence dans le plan
Soit $\Omega\subset \mathbb{R} ^2$ régulier, $F\in C^1(\Omega;\mathbb{R}^2)$ un champ vectoriel, et $\nu$ la normale extérieure à $Ω$. Alors, nous avons:
$$\iint_{\Omega}\operatorname{div}F\ dxdy=\int_{\partial\Omega}(F\bullet\nu) d\ell $$
##### Remarque 2.22
1. Si $\Gamma$ est une partie du bord de $\Omega$ de paramétrisation $\gamma: [ a, b] \to \mathbb R^2$ on a: 
$$
\int_{\Gamma}F\bullet\nu d\ell=\int_a^bF(\gamma(t))\bullet(\gamma_2^{\prime}(t),-\gamma_1^{\prime}(t))dt
$$
2. Si $\Gamma$ est une partie du bord sur laquelle je connais la normale extérieure, $\int_\Gamma F\bullet\nu d\ell$ ne dépend pas du sens de parcours car c'est l'intégrale curviligne d'un champs scalaire.
	Quelques courbes dont on connait la normale:
	- les segments de droite
	- les arcs de cercle centrés en 0

##### Corollaire 2.24: Formules d'aire:
Soit $\Omega \subseteq \mathbb R^2$ un domaine régulier, $F, G_1, G_2 \in C^\infty(\bar\Omega; \mathbb R^2)$ définis par:
$F(x,y) = (-y,x); G_1(x,y) = (0,x); G_2(x,y) = (-y, 0)$. Alors:
$$
\text{Aire}(\Omega) = \iint_\Omega dx dy = \frac 1 2 \int_{\partial \Omega} F \bullet d\ell = \int_{\partial \Omega} G_1 \bullet d\ell= \int_{\partial \Omega} G_2 \bullet d\ell
$$
##### Corollaire 2.25: Identités de Green dans $\mathbb R^2$:
Soit $\Omega \subseteq \mathbb R^2$ un domaine régulier, $\nu$ sa normale extérieure unité et $u,v\in C^2(\bar\Omega)$

$$ 
\
\iint_{\Omega}\Delta u(x,y)dxdy=\int_{\partial\Omega}\left(\nabla u\cdot\nu\right)d\ell. 
$$

$$
 
\iint_{\Omega}\left[v(x,y)\Delta u(x,y)+(\nabla u(x,y)\cdot\nabla v(x,y))\right]dxdy = \int_{\partial\Omega}\left(v\nabla u\cdot\nu\right)dl
$$

$$

\iint_{\Omega}\left(u(x,y)\Delta v(x,y)-v(x,y)\Delta u(x,y)\right)dxdy= \int_{\partial\Omega}\left[u\left(\nabla v\cdot\nu\right)-v\left(\nabla u\cdot\nu\right)\right]dl
$$

